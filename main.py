"""A Kubernetes Python Pulumi program"""
from collections import OrderedDict
import json
import os
import sys

import pulumi
from pulumi_kubernetes.apps.v1 import Deployment, DeploymentSpecArgs
from pulumi_kubernetes.meta.v1 import LabelSelectorArgs, ObjectMetaArgs
from pulumi_kubernetes.core.v1 import ContainerArgs, PodSpecArgs, PodTemplateSpecArgs
from pulumi_kubernetes.yaml import ConfigGroup
from pulumi import automation as auto
import requests


PULUMI_KUBERNETES_VERSION="4.15.0"

TEMPLATE_DNS = """
apiVersion: externaldns.k8s.io/v1alpha1
kind: DNSEndpoint
metadata:
  name: unifi-{}
  namespace: system
spec:
  endpoints:
{}
"""

TEMPLATE_ENDPOINT = """  - dnsName: {}
    recordTTL: 180
    recordType: A
    targets:
    - {}
"""


# This is the pulumi program in "inline function" form
def pulumi_program():
    unifi_host = os.environ.get("UNIFI_HOST")
    unifi_port = int(os.environ.get("UNIFI_PORT", 8443))
    unifi_username = os.environ.get("UNIFI_USERNAME")
    unifi_password = os.environ.get("UNIFI_PASSWORD")
    unifi_site = os.environ.get("UNIFI_SITE", "default")
    base_domain = os.environ.get("DOMAIN", ".local")
    only_fqdn = os.environ.get("ONLY_FQDN", False)
    if only_fqdn.lower() == "false":
        only_fqdn = False

    # Get unifi client
    custom_domains = {}
    headers = {"Accept": "application/json", "Content-Type": "application/json"}
    login_url = f"https://{unifi_host}:{unifi_port}/api/auth/login"
    data = {"username": unifi_username, "password": unifi_password}
    http_session = requests.Session()
    resp = http_session.post(login_url, headers=headers, json=data, verify=False)
    if resp.status_code != 200:
        print("ERROR: Can not login to unifi")
        return

    client_url = (
        f"https://{unifi_host}:{unifi_port}/proxy/network/api/s/{unifi_site}/rest/user"
    )
    resp = http_session.get(client_url, headers=headers, verify=False)

    if resp.status_code != 200:
        print("ERROR: Can not get unifi configured client")
        return

    clients = resp.json()["data"]
    for client in clients:
        if client.get("fixed_ip") in [None, ""]:
            print(
                "WARNING: no fixed IP - ignoring client: {} - {} - {} ".format(
                    client.get("mac"), client.get("last_ip"), client.get("hostname")
                )
            )
            continue
        elif client.get("name") in [None, ""]:
            print(
                "WARNING: no name - ignoring client: {} -{} - {} ".format(
                    client.get("mac"), client.get("last_ip"), client.get("hostname")
                )
            )
            continue
        else:
            ip = client["fixed_ip"]
            domain = client["name"]
            if domain in custom_domains and custom_domains[domain] != ip:
                print(
                    "ERROR: duplication detected {} has 2 ips: {}, {}".format(
                        domain, ip, custom_domains[domain]
                    )
                )
                continue
            elif domain in custom_domains and custom_domains[domain] == ip:
                print("WARNING: duplication detected {}: {}".format(domain, ip))
                continue
            custom_domains[domain] = ip

    custom_domains_ordered = OrderedDict(sorted(custom_domains.items()))

    # Create k8s crd objects
    for hostname, ip in custom_domains_ordered.items():
        dns_names = [f"{hostname}.{base_domain}"]
        if not only_fqdn:
            dns_names.append(hostname)
        endpoints = ""
        for dns_name in dns_names:
            endpoints += TEMPLATE_ENDPOINT.format(dns_name.lower(), ip)

        yaml = TEMPLATE_DNS.format(hostname.lower(), endpoints)

        ConfigGroup(hostname, yaml=[yaml])


def init_stack():
    project_name = os.environ["PULUMI_PROJECT"]
    # We use a simple stack name here, but recommend using auto.fully_qualified_stack_name for maximum specificity.
    stack_name = os.environ["PULUMI_STACK"]
    # stack_name = auto.fully_qualified_stack_name("myOrgOrUser", project_name, stack_name)

    # create or select a stack matching the specified name and project.
    # this will set up a workspace with everything necessary to run our inline program (pulumi_program)
    stack = auto.create_or_select_stack(
        stack_name=stack_name, project_name=project_name, program=pulumi_program
    )

    print("successfully initialized stack")

    # for inline programs, we must manage plugins ourselves
    print("installing plugins...")
    stack.workspace.install_plugin("kubernetes", f"v{PULUMI_KUBERNETES_VERSION}")
    print("plugins installed")

    return stack


def main():
    requests.packages.urllib3.disable_warnings()
    stack = init_stack()

    args = sys.argv[1:]

    refresh = False
    preview = False
    up_ = False
    destroy = False

    if len(args) > 0:
        if args[0] == "preview":
            preview = True
        elif args[0] == "refresh":
            refresh = True
        elif args[0] == "up":
            up_ = True
        elif args[0] == "destroy":
            destroy = True

    if len(args) > 1:
        if args[1] == "refresh":
            refresh = True

    if refresh:
        print("refreshing stack...")
        stack.refresh(on_output=print)
        print("refresh complete")

    if preview:
        print("preview stack...")
        stack.preview(on_output=print)

    elif up_:
        print("updating stack...")
        up_res = stack.up(on_output=print)
        print(
            f"update summary: \n{json.dumps(up_res.summary.resource_changes, indent=4)}"
        )

    elif destroy:
        print("destroying stack...")
        stack.destroy(on_output=print)
        print("stack destroy complete")


if __name__ == "__main__":
    main()
