FROM pulumi/pulumi-python:3.127.0

RUN apt update && apt install python3 python3-pip -y

WORKDIR /iac

ADD main.py requirements.txt /iac/

RUN pip3 install -r requirements.txt
